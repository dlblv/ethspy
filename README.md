# EthSpy

## Description

A small application that listens Ethereum network for new blocks and prints transactions with amounts exceeding the threshold given

## Usage
`cargo run -- AMOUNT [RPC_ENDPOINT]`, where `AMOUNT` is a threshold amount in ETH and `RPC_ENDPOINT` is Ethereum JSON RPC API endpont. By default `eth.llamarpc.com` is used. Endpoint can be also specified via `ETH_RPC_EP` environmental variable
