use anyhow::{anyhow, Result};
use clap::Parser;
use futures_util::{StreamExt, TryFutureExt};
use jsonrpsee::{
    core::client::{Subscription, SubscriptionClientT},
    http_client::{HttpClient, HttpClientBuilder},
    rpc_params,
    ws_client::WsClientBuilder,
};
use reth_primitives::{constants::ETH_TO_WEI, U256};
use reth_rpc_api::EthApiClient;
use reth_rpc_types::{BlockTransactions, Header};
use rug::{integer::Order, Float};

#[derive(Debug, Parser)]
struct Args {
    /// Amount of ETH in transfers we're looking for
    amount: f64,

    /// Ethereum RPC endpoint
    #[clap(default_value_t = String::from("eth.llamarpc.com"), env = "ETH_RPC_EP")]
    rpc_api_ep: String,
}

#[tokio::main]
async fn main() -> Result<()> {
    let Args { amount, rpc_api_ep } = Args::parse();

    let amount = {
        let amount = Float::with_val(256, amount) * ETH_TO_WEI;
        let amount = amount
            .to_integer()
            .ok_or(anyhow!("amount is not a finite number!"))?;

        let (res, of) = U256::overflowing_from_limbs_slice(&amount.to_digits(Order::Lsf));

        if of {
            return Err(anyhow!("U256 overflow occurred on ETH to WEI conversion!"));
        }

        res
    };

    let ws_client = WsClientBuilder::default()
        .build(format!("wss://{rpc_api_ep}"))
        .await
        .map_err(|e| anyhow!("failed to build WS client: {e}"))?;

    let http_client = HttpClientBuilder::default()
        .build(format!("https://{rpc_api_ep}"))
        .map_err(|e| anyhow!("failed to build HTTP client: {e}"))?;

    let sub: Subscription<Header> = ws_client
        .subscribe("eth_subscribe", rpc_params!("newHeads"), "eth_unsubscribe")
        .await
        .map_err(|e| anyhow!("WS subscription failed: {e}"))?;

    sub.for_each(|header| async {
        if let Err(e) = async { header.map_err(Into::into) }
            .and_then(|header| process_header(&http_client, header, amount))
            .await
        {
            eprintln!("Header processing error: {e}");
        }
    })
    .await;

    Ok(())
}

async fn process_header(http_client: &HttpClient, header: Header, amount: U256) -> Result<()> {
    let block_hash = header
        .hash
        .ok_or_else(|| anyhow!("empty block hash! {header:?}"))?;
    let block = http_client
        .block_by_hash(block_hash, true)
        .await?
        .ok_or(anyhow!("block not found: {block_hash}!"))?;
    let transactions = match &block.transactions {
        BlockTransactions::Full(txs) => txs,
        _ => return Err(anyhow!("loaded block does not have full tx info!")),
    };
    for tx in transactions
        .iter()
        .filter(|tx| tx.to.is_some() && tx.value >= amount)
    {
        println!(
            "{} -> {} | {}",
            tx.from,
            tx.to.unwrap(), // unwrap is safe cause we've checked that tx.to is Some
            tx.value
        );
    }
    Ok(())
}
